<?php

class Figure {
    protected $isBlack;

    public function __construct($isBlack) {
        $this->isBlack = $isBlack;
    }

    /**
     * Возвращает цвет фигуры
     * Белые - true
     * Чёрные - false, то есть инверсно isBlack
     *
     * @return bool
     */
    public function getColor(){
        return !$this->isBlack;
    }

    /** @noinspection PhpToStringReturnInspection */
    public function __toString() {
        throw new \Exception("Not implemented");
    }
}
