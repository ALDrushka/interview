<?php

class Desk {
    private $figures = [];
    private $log = [];

    public function __construct() {
        $this->figures['a'][1] = new Rook(false);
        $this->figures['b'][1] = new Knight(false);
        $this->figures['c'][1] = new Bishop(false);
        $this->figures['d'][1] = new Queen(false);
        $this->figures['e'][1] = new King(false);
        $this->figures['f'][1] = new Bishop(false);
        $this->figures['g'][1] = new Knight(false);
        $this->figures['h'][1] = new Rook(false);

        $this->figures['a'][2] = new Pawn(false);
        $this->figures['b'][2] = new Pawn(false);
        $this->figures['c'][2] = new Pawn(false);
        $this->figures['d'][2] = new Pawn(false);
        $this->figures['e'][2] = new Pawn(false);
        $this->figures['f'][2] = new Pawn(false);
        $this->figures['g'][2] = new Pawn(false);
        $this->figures['h'][2] = new Pawn(false);

        $this->figures['a'][7] = new Pawn(true);
        $this->figures['b'][7] = new Pawn(true);
        $this->figures['c'][7] = new Pawn(true);
        $this->figures['d'][7] = new Pawn(true);
        $this->figures['e'][7] = new Pawn(true);
        $this->figures['f'][7] = new Pawn(true);
        $this->figures['g'][7] = new Pawn(true);
        $this->figures['h'][7] = new Pawn(true);

        $this->figures['a'][8] = new Rook(true);
        $this->figures['b'][8] = new Knight(true);
        $this->figures['c'][8] = new Bishop(true);
        $this->figures['d'][8] = new Queen(true);
        $this->figures['e'][8] = new King(true);
        $this->figures['f'][8] = new Bishop(true);
        $this->figures['g'][8] = new Knight(true);
        $this->figures['h'][8] = new Rook(true);
    }

    public function move($move) {
        if (!preg_match('/^([a-h])(\d)-([a-h])(\d)$/', $move, $match)) {
            throw new \Exception("Incorrect move");
        }

        $xFrom = $match[1];
        $yFrom = $match[2];
        $xTo   = $match[3];
        $yTo   = $match[4];

        if (isset($this->figures[$xFrom][$yFrom])) {
            //получаем цвет текущего хода
            $colorMove = $this->figures[$xFrom][$yFrom]->getColor() ? "white" : "black";

            //Проверяем не ходят ли случайно белые либо чёрные 2 раза
            if(!empty($this->log)){
                //Получаем цвет последнего хода
                $lastMoveColor = array_key_last($this->log[array_key_last($this->log)]);

                //Удвоение ходов - один и тот же цвет ходит дважды
                if($colorMove == $lastMoveColor){
                    throw new \Exception("$move Incorrect move. Doubling color of the turn");
                }
            }

            //Проверка валидности хода для пешки
            if($this->figures[$xFrom][$yFrom] instanceof Pawn){
                $xArr = ['a' => 0, 'b' => 1, 'c' => 2, 'd' => 3, 'e' => 4, 'f' => 5, 'g' => 6, 'h' => 7];

                //Проверяем допустимый ли ход пешки:
                // 1. Пешка ходит вперёд на 1 прямо и впереди нет фигуры
                // 2. Пешка ходит вперёд на 2 и впереди нет фигуры, нет фигуры в клетке, которую перескакивает
                // 3. Пешка бьёт на 1 клетку по диагонали - там есть фигура
                // Все остальные ходы - недопустимы, выбрасываем исключение

                switch ($colorMove){
                    case "white":
                        if($yTo - $yFrom == 1 && $xTo == $xFrom && !isset($this->figures[$xTo][$yTo]) ||
                            $yFrom == 2 && $yTo - $yFrom == 2 && $xTo == $xFrom &&
                            !isset($this->figures[$xTo][$yTo-1]) && !isset($this->figures[$xTo][$yTo]) ||
                            isset($this->figures[$xTo][$yTo]) && $yTo - $yFrom == 1 && abs($xArr[$xTo] - $xArr[$xFrom]) == 1)
                        {
                            break;
                        }

                        throw new \Exception("$move Incorrect move pawn");

                    //Аналогично для чёрных. Чтобы не запутаться сделал 2 условия, в дальнейшем можно сократить условия до одного
                    case "black":
                        if($yTo - $yFrom == -1 && $xTo == $xFrom && !isset($this->figures[$xTo][$yTo]) ||
                            $yFrom == 7 && $yTo - $yFrom == -2 && $xTo == $xFrom &&
                            !isset($this->figures[$xTo][$yTo+1]) && !isset($this->figures[$xTo][$yTo]) ||
                            isset($this->figures[$xTo][$yTo]) && $yTo - $yFrom == -1 && abs($xArr[$xTo] - $xArr[$xFrom]) == 1)
                        {
                            break;
                        }

                        throw new \Exception("$move Incorrect move pawn");
                    default :
                        throw new \Exception("$move Incorrect color of pawn");
                }
            }

            $this->figures[$xTo][$yTo] = $this->figures[$xFrom][$yFrom];

            //Сохраняем текущий ход
            $this->log[] = [$colorMove => $move];
        }
        unset($this->figures[$xFrom][$yFrom]);
    }

    public function dump() {
        for ($y = 8; $y >= 1; $y--) {
            echo "$y ";
            for ($x = 'a'; $x <= 'h'; $x++) {
                if (isset($this->figures[$x][$y])) {
                    echo $this->figures[$x][$y];
                } else {
                    echo '-';
                }
            }
            echo "\n";
        }
        echo "  abcdefgh\n";
    }
}
